// Require model so we could use the model for searching
const User = require("../models/User.js");
const bcrypt = require("bcrypt");

// Require auth.js
const auth = require("../auth.js");


module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from all the documents' collection
	return User.find({email: reqBody.email }).then(result => {
		// condition if there is an exsiting  user
		if(result.length > 0){
			return true;
		}
		// condition if there is no existing user
		else
		{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,

		// bcrypt - package for password hashing 
		// hashSync() - synchronously generates a hash
		// hash() -  asynchronously generates a hash
		password : bcrypt.hashSync(reqBody.password, 10)
		// 10 = salt rounds
		// Salt rounds refer to hashing rounds, the higher salt rounds, the more hashing rounds, the longer it takes to generate an output.
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// module for login
// connected to auth.js for token creation
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			// compareSync is a bcrypt function to compare a hashed password to unhashed password
			// reqBody.password - hashed password from the database
			// result.password - unhashed password from user input
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				//Generate an access token
				return  {access : auth.createAccessToken(result)}
			}
			else{
				// If password do not match
				return false;
				// return "Incorrect password!";
			}
		}
	})
}

// Activity
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	})
}



